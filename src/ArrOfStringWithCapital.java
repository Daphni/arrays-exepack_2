public class ArrOfStringWithCapital {
    public static void main(String[] args) {
        String[] arr = {"Hello", "world", "have", "A", "nice", "day"};
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (Character.isUpperCase(arr[i].charAt(0))) {
                counter = counter + 1;
            }
        }
        System.out.println(counter);
    }
}
