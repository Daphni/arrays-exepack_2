public class PerOfBelowAver {
    public static void main(String[] args) {
        int[] arr = {2,5,18,20,100,54,69};
        int sum = 0;
        float aver = 0;
        int counter = 0;
        for (int i = 0; i<arr.length; i++){
            sum = sum + arr[i];
        }
        aver =  sum / arr.length;
        for(int i = 0; i<arr.length; i++){
            if (arr[i]< aver){
                counter = counter + 1;
            }
        }
        System.out.println("The percentage of the elements that are below the average value is "+ counter*100/arr.length+"%");
    }

}
