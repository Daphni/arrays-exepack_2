public class SpitToTwoArr {
    public static void main(String[] args) {
        int[] arr1 = {2, 5, 18, 20, 100, 54, 69};
        int help = 0;
        for (int i = 1; i < 7; i++) {
            if (arr1[i] < arr1[i - 1]) {
                help = arr1[i];
                arr1[i] = arr1[i - 1];
                arr1[i - 1] = help;
            }
        }
        help = 0;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] < 29) {
                help = help + 1;
            }
        }
        int arr2[] = new int[help];
        int arr3[] = new int[arr1.length - help];
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = arr1[i];
        }
        for (int i = 1; i < arr3.length+1; i++) {
            arr3[arr3.length - i] = arr1[arr1.length - i];
        }
        System.out.println("The first array");
        for (int i = 0; i < arr2.length; i++) {
            System.out.println(arr2[i]);
        }
        System.out.println("The second array");
        for (int i = 0; i < arr3.length; i++) {
            System.out.println(arr3[i]);
        }

    }
}
