public class SumAndAverage {
    public static void main(String[] args) {
        int[] array = {2, 5, 18, 20, 100, 54, 69};
        int sum = 0;
        for (int i = 0; i <array.length; i++) {
            sum = sum + array[i];
        }
        System.out.println(sum);
        System.out.println((float) sum / array.length);
    }
}
